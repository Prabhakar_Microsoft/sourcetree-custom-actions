#!/bin/bash

# Based on https://github.com/knalli/sourcetree-custom-actions/blob/master/Github/Open_commit_on_Github.sh
# Opens the BitBucket page for the current git commit

source $(dirname $0)/bitbucket_base.sh

open "$(bitbucket_base_url)/commits/$1"
