@echo off
REM setlocal enableDelayedExpansion

for /f "delims=" %%i in ('git config --get remote.origin.url') do set sourceUrl=%%i
REM set sourceUrl=https://bitbucket.org/Get-Started/sourcetree-custom-actions.git
REM set sourceUrl=https://username@bitbucket.org/Get-Started/sourcetree-custom-actions.git

set baseUrl=%sourceUrl%
set baseUrl=%baseUrl:.git=%

for /f "tokens=1,2 delims=@" %%a in ("%baseUrl%") do set uname=%%a&set upath=%%b
set base=%upath%

IF "%base%"=="" set base=%uname%
set base=%base:https://=%

echo https://%base%