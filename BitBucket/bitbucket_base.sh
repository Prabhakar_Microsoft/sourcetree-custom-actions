#!/bin/bash

# Based on https://github.com/knalli/sourcetree-custom-actions/blob/master/Github/Open_commit_on_Github.sh
# Converts the origin URL (git endpoint) to the project URL on BitBucket
# Source format: https://domain.com/git/scm/foo-project/repo-name.git
# Target format: https://domain.com/git/projects/foo-project/repos/repo-name

function bitbucket_base_url() {
  local giturl=$(git config --get remote.origin.url)

  if [ "$giturl" == "" ]
    then
     echo "Not a git repository or no remote.origin.url set"
     exit 1;
  fi


  re='(.*)@(.*)/(.*)/(.*)\.git'

  if [[ $giturl =~ $re ]]
  then
    giturl=${BASH_REMATCH[2]}/${BASH_REMATCH[3]}/${BASH_REMATCH[4]}

    echo "http://"$giturl
  else
    echo "Git URL does not match expected pattern: $giturl"
    exit 1;
  fi
}
