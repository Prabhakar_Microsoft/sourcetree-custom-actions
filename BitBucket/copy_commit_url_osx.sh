#!/bin/bash

source $(dirname $0)/bitbucket_base.sh

echo "$(bitbucket_base_url)/commits/$1" | pbcopy
